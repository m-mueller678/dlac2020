## simple Calculator with Suppot for Lambda Calculus style Functions

* arithmetic expressions with \+ \- \* \/
* integer literals
* function definitions
* call functions with \\ operator
* built in _if_ function

run with _make run_ and type your programm (hit CTRL-D to signal end of programm)

## Examples
    x {x*x}                                                           // returns square of x
    x {x*x} \ 7                                                       // returns 49
    
    condition { a { b { if\condition\x{a}\b }}}                       // returns a if condition > 0, b otherwise
    condition { a { b { if\condition\x{a}\b }}} \1\7\13               // returns 7
    condition { a { b { if\condition\x{a}\b }}} \0\7\13               // returns 13
    
    x{x\x} \ pow { b { e { if\e\x{(b* pow\pow\b\(e-1) )}\1 }}} \2\128 // returns 2^128
