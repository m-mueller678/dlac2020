character ="a"|"b"|"c"|"d"|"e"|"f"|"g"|"h"|"i"|"j"|"k"|"l"|"m"|"n"|"o"|"p"|"q"|"r"|"s"|"t"|"u"|"v"|"w"|"x"|"y"|"z";
positiveDigit = "1"|"2"|"3"|"4"|"5"|"6"|"7"|"8"|"9";
op2 ="*" | "/";
op3 ="+" | "-";
identifier =character,{character};
positiveNumber = positiveDigit, {"0" | positiveDigit};
nonNegativeNumber =positiveNumber | "0";
expr0 = ["-"], ( nonNegativeNumber|identifier, [ "{",  expr3, "}" ] | "(", expr3, ")" );
expr1 =expr0, {"\", expr0};
expr2 =expr1, {op2, expr1};
expr3 =expr2, {op3, expr2};

syntax=expr3;
