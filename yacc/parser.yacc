%{
#include "../src/ast.h"
#include <stdio.h>
#include <string.h>

int yylex(void);
int yyparse(void);

void yyerror(const char *str){
	fprintf(stderr,"error: %s\n",str);
}

int yywrap(){
	return 1;
}

static AstNode* parse_result;

AstNode* get_parsed_ast(){
	assert(yyparse()==0,"parsing failed");
	printf("parsing complete\n");
	ast_node_print(stdout,parse_result);
	return parse_result;
}

%}



%token POPEN PCLOSE FOPEN FCLOSE FCALL PLUS MINUS DIV MUL DIGITS IDENT


%%

syntax:expr3{
	parse_result=$1;
}

expr3:expr2
	|expr3 PLUS expr2{
		$$ = ast_node_alloc_bin_op($1,AstOperatorAdd,$3);
	}
	|expr3 MINUS expr2{
		$$ = ast_node_alloc_bin_op($1,AstOperatorSub,$3);
	}

expr2:expr1
	|expr2 MUL expr1{
		$$ = ast_node_alloc_bin_op($1,AstOperatorMul,$3);
	}
	|expr2 DIV expr1{
		$$ = ast_node_alloc_bin_op($1,AstOperatorDiv,$3);
	}

expr1:expr0
	|expr1 FCALL expr0{
		$$ = ast_node_alloc_bin_op($1,AstOperatorCall,$3);
	}

expr0:posexpr0
	|MINUS posexpr0 {
		$$ = ast_node_alloc_bin_op(ast_node_alloc_number(0),AstOperatorSub,$2);
	}

posexpr0:DIGITS
	|definition
	|IDENT
	|POPEN expr3 PCLOSE {$$=$2;}

definition:IDENT FOPEN expr3 FCLOSE{
	$$ = ast_node_alloc_definition($1,$3);
}

