CC := gcc
LFLAG := -std=c1x -g
CFLAG := -c -g
WARNFLAG := -Wall -Wextra  -pedantic

SRC_PATH := src
OBJ_PATH :=build

TARGET := build/out

# src files & obj files
SRC := $(foreach x, $(SRC_PATH), $(wildcard $(addprefix $(x)/*,.c*)))
OBJ := $(addprefix $(OBJ_PATH)/, $(addsuffix .o, $(notdir $(basename $(SRC)))))

CLEAN_LIST := $(TARGET) \
		$(OBJ)\
		flex/scanner.o flex/scanner.c \
		yacc/y.tab.h yacc/y.tab.c yacc/y.tab.o
default: all


$(OBJ_PATH)/%.o: $(SRC_PATH)/%.c
	mkdir -p $(OBJ_PATH)
	$(CC) $(CFLAG) $(WARNFLAG) -o $@ $<

$(TARGET): $(OBJ) flex/scanner.o yacc/y.tab.o
	$(CC) $(LFLAG) $(WARNFLAG) -o $@ $^


flex/scanner.o: flex/scanner.c yacc/y.tab.h
	$(CC) $(CFLAG) -o $@ $<

yacc/y.tab.o: yacc/y.tab.c
	$(CC) $(CFLAG) -o $@ $<

flex/scanner.c: flex/scanner.l
	flex -o $@ $<

yacc/y.tab.h yacc/y.tab.c : yacc/parser.yacc
	cd yacc; yacc -d parser.yacc

# phony rules
.PHONY: all
all: $(TARGET)

.PHONY: clean
clean:
	@echo CLEAN $(CLEAN_LIST)
	@rm -f $(CLEAN_LIST)

.PHONY: run
run: all
	$(TARGET)

