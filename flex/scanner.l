%{
#include "../src/ast.h"
#include "../src/util.h"
#include "../yacc/y.tab.h"


#define DEBUG_PRINT(...) if(0) fprintf(stderr,__VA_ARGS__)

%}
%%
\{ { DEBUG_PRINT("found token FOPEN\n");return FOPEN;}
\} { DEBUG_PRINT("found token FCLOSE\n");return FCLOSE;}
\( { DEBUG_PRINT("found token POPEN\n");return POPEN;}
\) { DEBUG_PRINT("found token PCLOSE\n");return PCLOSE;}
\+ { DEBUG_PRINT("found token PLUS\n");return PLUS;}
\- { DEBUG_PRINT("found token MINUS\n");return MINUS;}
\* { DEBUG_PRINT("found token MUL\n");return MUL;}
\/ { DEBUG_PRINT("found token DIV\n");return DIV;}
\\ { DEBUG_PRINT("found token FCALL\n");return FCALL;}
[0-9][0-9]* {
	uint64_t num=str_to_u64(yytext);
	DEBUG_PRINT("found token DIGITS: %"PRIu64"\n",num);
	yylval=ast_node_alloc_number(num);
	return DIGITS;
}
[a-z]+ {
	char* ident=ualloc(strlen(yytext)+1);
	strcpy(ident,yytext);
	DEBUG_PRINT("found token IDENT: %s\n",yytext);
	yylval=ast_node_alloc_ident(ident);
	return IDENT;
}
\n                      /* ignore end of line */;
[ \t]+                  /* ignore whitespace */;
%%
