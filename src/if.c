#include "if.h"
#include "ast.h"

static AstNode* identity_fn_body;

static void returns_id(Value* dst,Value v){
	value_destroy(&v);
	value_init_function(dst,identity_fn_body,NULL);
}

static void implementation(Value* dst,Value v){
	value_expect_type(&v,ValueTagNumber);
	double n;
	value_read_number(&v,&n);
	value_destroy(&v);
	if(n>0.0){
		printf("if true\n");
		value_init_function(dst,identity_fn_body,NULL);
	}else{
		printf("if false\n");
		value_init_native_function(dst,returns_id);
	}
}

NativeFunction* native_fn_if(){
	static int init=0;
	assert_here(!init);
	init=1;
	
	identity_fn_body=ualloc(sizeof(AstNode));
	identity_fn_body->tag=AstTagUpvalueRef;
	identity_fn_body->upvalue_ref_index=0;
	
	return implementation;
}
