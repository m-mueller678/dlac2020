#ifndef DLAC_AST_H
#define DLAC_AST_H

#define YYSTYPE AstNode *

#include "util.h"
#include "string_list.h"

typedef struct Value Value;
// v is owned
typedef void NativeFunction(Value* dst,Value v);

typedef struct AstNode AstNode;

typedef enum AstOperator{
	AstOperatorAdd,AstOperatorSub,AstOperatorMul,AstOperatorDiv,AstOperatorCall,
}AstOperator;

typedef enum AstNodeTag{
	AstTagNumber,
	AstTagIdentifier,
	AstTagBinaryOp,
	AstTagDefinition,
	AstTagParentFrameArgRef,
	AstTagUpvalueRef,
	AstTagNativeFunction,
}AstNodeTag;


typedef struct DefinitionAstNode DefinitionAstNode;

typedef struct FunctionInfo{
	StringList* free_names;
	int depth;
	DefinitionAstNode* parent_definition;
} FunctionInfo;

struct DefinitionAstNode{
	char* arg_name;
	AstNode * body;
	struct FunctionInfo* function_info;
};

struct AstNode{
	AstNodeTag tag;
	union{
		DefinitionAstNode definition;
		uint64_t number;
		char* identifier;
		DefinitionAstNode * parent_frame_arg_ref;
		int upvalue_ref_index;
		NativeFunction* native_function;
		struct{
			AstNode * lhs;
			AstOperator operator;
			AstNode * rhs;
		};
	};
};



// takes ownership of w
AstNode* ast_node_alloc_ident(char* w);
AstNode* ast_node_alloc_number(uint64_t n);
// takes ownership of op1 and op2
AstNode* ast_node_alloc_bin_op(AstNode* op1,AstOperator,AstNode* op2);
// takes ownership of name and body
AstNode* ast_node_alloc_definition(AstNode* argument,AstNode* body);

// name is borowed
AstNode* ast_push_native_function(AstNode* ast,char* name,NativeFunction f);

void ast_node_print(FILE*,AstNode*);

void ast_process(AstNode* ast);

AstNode* get_parsed_ast();

Value ast_evaluate(AstNode*);

#endif
