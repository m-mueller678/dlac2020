#include "ast.h"
#include "if.h"
#include "value.h"

int main(){
	AstNode* ast=get_parsed_ast();
	ast=ast_push_native_function(ast,"if",native_fn_if());
	ast_process(ast);
	Value result=ast_evaluate(ast);
	printf("result: ");
	value_print_short(&result);
	printf("\n");
}
