#ifndef DLAC_VALUE_H
#define DLAC_VALUE_H

#include "ast.h"

typedef enum ValueTag{
	ValueTagNumber,
	ValueTagFunction,
	ValueTagNativeFunction,
}ValueTag;

typedef struct UpValueChain UpValueChain;
typedef struct Value Value;

struct Value{
	enum ValueTag tag;
	union{
		double number;
		NativeFunction* native_function;
		struct{
			UpValueChain* upvalues;
			AstNode* body;
		};
	};
};

struct UpValueChain{
	int refcount;
	Value value;
	UpValueChain* next;
};

UpValueChain* upvalue_new();
//owned copy is created at dst
void upvalue_get(UpValueChain* upvalues,Value* dst,int index);
void upvalue_destroy(UpValueChain* v);
//does not consumes v or u
UpValueChain* upvalue_push(UpValueChain* u,Value* v);
void value_init_number(Value* v, double x);

//does not consume body or up
void value_init_function(Value* v,AstNode* body,UpValueChain* up);
void value_init_native_function(Value* v,NativeFunction* f);
void value_destroy(Value* value);
ValueTag value_type(Value* v);
void value_read_number(Value*v,double*dst);
// up is borrowed
void value_read_function(Value*v,AstNode** body,UpValueChain** up);
void value_read_native_function(Value*v,NativeFunction** dst);
void value_expect_type(Value* v,ValueTag expected);

void value_print_short(Value*v);
void upvalue_print_short(UpValueChain*v);

#endif
