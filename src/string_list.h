typedef struct StringList StringList;

StringList* string_list_empty();
//takes ownership of sl
StringList* string_list_push(StringList* sl,char* elem);
int string_list_find_ptr(StringList* l,char* ptr);
char* string_list_index(StringList* l,int index);
// pushes str to *l if &l does not contain str
// return 1 if pushed, 0 otherwise
int string_list_try_insert(StringList** l,char* str);

int string_list_is_empty(StringList* sl);
char* string_list_first(StringList* sl);
// use in combination with first and is_empty to iterate list
StringList* string_list_iter_next(StringList*);
