#ifndef DLAC_UTIL_H
#define DLAC_UTIL_H

#include <inttypes.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <stdio.h>
#include <stdio.h>
#include <errno.h>

void* ualloc(size_t size);

#define panic(...) do{\
	fprintf(stderr,"\npanic\nlast os error: %s\nmessage: ",strerror( errno ));\
	fprintf(stderr, __VA_ARGS__ );\
	exit(1);\
}while(0)

#define panic_here() panic("%s:%d\n",__FILE__,__LINE__)

#define assert_here(cond) assert(cond,"%s:%d\n",__FILE__,__LINE__)
#define assert(cond,...) do{ if (!(cond)) {panic(__VA_ARGS__);} }while(0)

uint64_t str_to_u64(char* str);

#endif
