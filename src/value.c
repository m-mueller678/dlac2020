#include "util.h"
#include "value.h"

UpValueChain* upvalue_new(){
	return NULL;
}

static void upvalue_inc(UpValueChain* up){
	if(up)
		up->refcount+=1;
}

static void value_inc(Value* v){
	switch (v->tag){
		case ValueTagNativeFunction:
		case ValueTagNumber:break;
		case ValueTagFunction:
			upvalue_inc(v->upvalues);
			break;
	}
}

void upvalue_get(UpValueChain* upvalues,Value* dst,int index){
	assert(upvalues,"upvalue index out of range\n");
	if(index==0){
		*dst=upvalues->value;
		value_inc(dst);
	}else{
		upvalue_get(upvalues->next,dst,index-1);
	}
}

void upvalue_destroy(UpValueChain* v){
	if(!v)
		return;
	v->refcount-=1;
	if(v->refcount>0)
		return;
	UpValueChain* next_v=v->next;
	free(v);
	upvalue_destroy(next_v);
}

UpValueChain* upvalue_push(UpValueChain* u,Value* v){
	upvalue_inc(u);
	value_inc(v);
	UpValueChain* new_chain=ualloc(sizeof(UpValueChain));
	new_chain->refcount=1;
	new_chain->value=*v;
	new_chain->next=u;
	return new_chain;
}

void value_init_number(Value* v,double x){
	Value ret={.tag=ValueTagNumber,.number=x};
	*v=ret;
}

void value_init_function(Value* v,AstNode* body,UpValueChain* up){
	upvalue_inc(up);
	Value ret={
		.tag=ValueTagFunction,
		.body=body,
		.upvalues=up,
	};
	*v=ret;
}

void value_init_native_function(Value* v,NativeFunction* f){
	Value ret={
		.tag=ValueTagNativeFunction,
		.native_function=f,
	};
	*v=ret;
}

void value_destroy(Value* value){
	switch(value->tag){
		case ValueTagNativeFunction:
		case ValueTagNumber:break;
		case ValueTagFunction:
			upvalue_destroy(value->upvalues);
			break;
	}
}

ValueTag value_type(Value* v){
	return v->tag;
}

static char const* type_name(ValueTag t){
	static char const *const names[]={
		"Number",
		"Function",
		"NativeFunction"
	};
	return names[t];
}

void value_expect_type(Value* v,ValueTag expected){
	assert(v->tag==expected,"expected type %s, found %s\n",type_name(expected),type_name(v->tag));
}

void value_read_number(Value*v,double*dst){
	value_expect_type(v,ValueTagNumber);
	*dst=v->number;
}

void value_read_function(Value*v,AstNode** body,UpValueChain** up){
	value_expect_type(v,ValueTagFunction);
	*body=v->body;
	*up=v->upvalues;
}

void value_read_native_function(Value*v,NativeFunction** dst){
	value_expect_type(v,ValueTagNativeFunction);
	*dst=v->native_function;
}

void value_print_short(Value*v){
	switch(v->tag){
		case ValueTagNumber:
			printf("%f",v->number);
			break;
		case ValueTagFunction:
			printf("<function>");
			break;
		case ValueTagNativeFunction:
			printf("<native function>");
			break;
	}
}

void upvalue_print_short(UpValueChain*up){
	while(up){
		value_print_short(&up->value);
		printf(", ");
		up=up->next;
	}
}
