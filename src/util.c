#include "util.h"

void* ualloc(size_t size){
	void* ptr=malloc(size);
	if(!ptr)
		panic("alloc failed");
	return ptr;
}

uint64_t str_to_u64(char* str){
	char* endptr;
	uint64_t result=strtoull(str,&endptr,10);
	assert( (*str=='0' || result!=0) && result< UINTMAX_MAX ,"cannot convert to number: %s\n",str);
	return result;
}
