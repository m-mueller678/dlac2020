#include "string_list.h"
#include "util.h"

struct StringList{
	char* str;
	struct StringList* next;
};

StringList* string_list_empty(){
	return NULL;
}

StringList* string_list_push(StringList* sl,char* elem){
	StringList* new_list=ualloc(sizeof(StringList));
	new_list->str=elem;
	new_list->next=sl;
	return new_list;
}

int string_list_find_rec(StringList* l,char* ptr,int si){
	if(l==NULL)
		return -1;
	if(strcmp(l->str,ptr)==0)
		return si;
	return string_list_find_rec(l->next,ptr,si+1);
}

int string_list_find(StringList* l,char* ptr){
	return string_list_find_rec(l,ptr,0);
}

char* string_list_index(StringList* l,int index){
	assert(l,"stringlist index out of range");
	if(index==0)
		return l->str;
	return string_list_index(l->next,index-1);
}

int string_list_try_insert(StringList** l,char* str){
	if (string_list_find(*l,str)==-1){
		*l=string_list_push(*l,str);
		return 1;
	}else{
		return 0;
	}
}

int string_list_is_empty(StringList* sl){
	return sl==NULL;
}

char* string_list_first(StringList* sl){
	return sl->str;
}

StringList* string_list_iter_next(StringList* sl){
	return sl->next;
}
