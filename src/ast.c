#include "ast.h"
#include "util.h"
#include "value.h"

static const int MAX_CALL_DEPTH=5000;

typedef enum AstPrintFlags{
	AstPrintIncludeFi=1,
	AstPrintIncludeRef=2,
}AstPrintFlags;

static void print_node_indent(FILE* file,AstNode* node,int indent,AstPrintFlags options);

static FunctionInfo* alloc_function_info(DefinitionAstNode* parent_definition){
	FunctionInfo* ptr=ualloc(sizeof(FunctionInfo));
	if (parent_definition)
		ptr->depth=parent_definition->function_info->depth+1;
	else
		ptr->depth=0;
	ptr->free_names=NULL;
	ptr->parent_definition=parent_definition;
	return ptr;
}

// writes function info
static void function_info_pass(AstNode* this,DefinitionAstNode* parent_frame){
	switch(this->tag){
		case AstTagNumber:
		case AstTagNativeFunction:
		case AstTagIdentifier:break;
		case AstTagBinaryOp:
			function_info_pass(this->lhs,parent_frame);
			function_info_pass(this->rhs,parent_frame);
			break;
		case AstTagDefinition:{
			//initialize function info
			FunctionInfo* fi=alloc_function_info(parent_frame);
			this->definition.function_info=fi;
			//pass children
			function_info_pass(this->definition.body,&this->definition);
			break;
		}
		case AstTagUpvalueRef:
		case AstTagParentFrameArgRef:
			panic_here();
	}
}

//resolves identifiers
static void resolve_pass(AstNode* this,DefinitionAstNode* parent_frame){
	switch(this->tag){
		case AstTagNativeFunction:
		case AstTagNumber:break;
		case AstTagIdentifier:
			while(1){
				assert(parent_frame,"could not resolve identifier: %s\n",this->identifier);
				if(strcmp(parent_frame->arg_name,this->identifier)==0){
					free(this->identifier);
					this->tag=AstTagParentFrameArgRef;
					this->parent_frame_arg_ref=parent_frame;
					break;
				}
				parent_frame=parent_frame->function_info->parent_definition;
			}
			break;
		case AstTagBinaryOp:
			resolve_pass(this->lhs,parent_frame);
			resolve_pass(this->rhs,parent_frame);
			break;
		case AstTagDefinition:{
			DefinitionAstNode* def=&this->definition;
			resolve_pass(def->body,def);
			break;
		}
		case AstTagUpvalueRef:
		case AstTagParentFrameArgRef:
			panic_here();
	}
}

static void upvalue_indexing_pass(AstNode* this,int depth){
	switch(this->tag){
		case AstTagNativeFunction:
		case AstTagNumber:break;
		case AstTagIdentifier:panic_here();
		case AstTagBinaryOp:
			upvalue_indexing_pass(this->lhs,depth);
			upvalue_indexing_pass(this->rhs,depth);
			break;
		case AstTagDefinition:
			upvalue_indexing_pass(this->definition.body,depth+1);
			break;
		case AstTagParentFrameArgRef:{
			this->tag=AstTagUpvalueRef;
			this->upvalue_ref_index=depth-this->parent_frame_arg_ref->function_info->depth;
			break;
		}
		case AstTagUpvalueRef:
			panic_here();
	}
}

void ast_process(AstNode* ast){
	printf("starting AST processing\n");
	
	function_info_pass(ast,NULL);
	printf("function info pass complete\n");
	print_node_indent(stdout,ast,0,AstPrintIncludeFi);
	
	resolve_pass(ast,NULL);
	printf("resolve pass complete\n");
	print_node_indent(stdout,ast,0,AstPrintIncludeFi|AstPrintIncludeRef);
	
	upvalue_indexing_pass(ast,-1);
	printf("upvalue indexing complete\n");
	print_node_indent(stdout,ast,0,0);
	
}

AstNode* ast_node_alloc_ident(char* w){
	AstNode* node=ualloc(sizeof(AstNode));
	node->tag=AstTagIdentifier;
	node->identifier=w;
	return node;
}

AstNode* ast_node_alloc_number(uint64_t n){
	AstNode* node=ualloc(sizeof(AstNode));
	node->tag=AstTagNumber;
	node->number=n;
	return node;
}
AstNode* ast_node_alloc_bin_op(AstNode* op1,AstOperator operator,AstNode* op2){
	AstNode* node=ualloc(sizeof(AstNode));
	node->tag=AstTagBinaryOp;
	node->lhs=op1;
	node->operator=operator;
	node->rhs=op2;
	return node;
}
AstNode* ast_node_alloc_definition(AstNode* argument,AstNode* body){
	assert(argument->tag==AstTagIdentifier,"ast_node_alloc_definition: argument is not an identifier");
	AstNode* node=ualloc(sizeof(AstNode));
	node->tag=AstTagDefinition;
	node->definition.body=body;
	node->definition.arg_name=argument->identifier;
	node->definition.function_info=NULL;
	free(argument);
	return node;
}

static void write_indent(FILE* file,int indent){
	for(int i=0;i<indent;++i)
		fprintf(file,"\t");
}

static void print_node_indent(FILE* file,AstNode* node,int indent,AstPrintFlags options){
	switch (node->tag){
		case AstTagNumber: fprintf(file,"%" PRIu64 "\n",node->number);break;
		case AstTagIdentifier: fprintf(file,"%s\n",node->identifier);break;
		case AstTagBinaryOp: {
			char op;
			switch (node->operator){
				case AstOperatorAdd:op='+';break;
				case AstOperatorSub:op='-';break;
				case AstOperatorMul:op='*';break;
				case AstOperatorDiv:op='/';break;
				case AstOperatorCall:op='\\';break;
			}
			fprintf(file,"\n");
			write_indent(file,indent+1);
			print_node_indent(file,node->lhs,indent+1,options);
			write_indent(file,indent);
			fprintf(file,"%c\n",op);
			write_indent(file,indent+1);
			print_node_indent(file,node->rhs,indent+1,options);
			break;
		}
		case AstTagDefinition: {
			FunctionInfo* fi=node->definition.function_info;
			//arg name
			fprintf(file,"%s",node->definition.arg_name);
			//address
			if (options&AstPrintIncludeRef)
				fprintf(file,"@%p",(void*)&node->definition);
			//function info
			if (fi && (options&AstPrintIncludeFi)){
				fprintf(file,"[%d",fi->depth);
				if(options&AstPrintIncludeRef)
					fprintf(file,"^%p",(void*)fi->parent_definition);
				fprintf(file,"]");
			}
			fprintf(file,"{\n");
			write_indent(file,indent+1);
			print_node_indent(file,node->definition.body,indent+1,options);
			write_indent(file,indent);
			fprintf(file,"}\n");
			break;
		case AstTagParentFrameArgRef:
			fprintf(file,"%s",node->parent_frame_arg_ref->arg_name);
			if (options&AstPrintIncludeRef)
				fprintf(file,"@%p",(void*)node->parent_frame_arg_ref);
			fprintf(file,"\n");
			break;
		case AstTagUpvalueRef:
			fprintf(file,"[^%d]\n",node->upvalue_ref_index);
			break;
		case AstTagNativeFunction:
			fprintf(file,"<native function at 0x%lx>\n",(size_t)node->native_function);
			break;
		}
	}
}

void ast_node_print(FILE* file,AstNode* node){
	print_node_indent(file,node,0,0);
}

static double evaluate_bin_op_numeric(double lhs,double rhs,AstOperator op){
	switch(op){
		case AstOperatorAdd:return lhs+rhs;
		case AstOperatorSub:return lhs-rhs;
		case AstOperatorMul:return lhs*rhs;
		case AstOperatorDiv:return lhs/rhs;
		case AstOperatorCall: panic_here();
	}
	panic_here();
}

static void evaluate_call(Value* dst,Value lhs,Value rhs,int call_depth);

//does not consume node or up
static void ast_evaluate_node(Value* dst,AstNode* node,UpValueChain* up,int call_depth){
	assert(call_depth<MAX_CALL_DEPTH,"call depth limit exceeded\n");
	switch(node->tag){
		case AstTagNumber:
			value_init_number(dst,node->number);
			break;
		case AstTagBinaryOp:{
			Value lhs;
			ast_evaluate_node(&lhs,node->lhs,up,call_depth+1);
			Value rhs;
			ast_evaluate_node(&rhs,node->rhs,up,call_depth+1);
			switch (node->operator){
				case AstOperatorAdd:
				case AstOperatorSub:
				case AstOperatorMul:
				case AstOperatorDiv:{
					double lhs_n,rhs_n;
					value_read_number(&lhs,&lhs_n);
					value_read_number(&rhs,&rhs_n);
					value_destroy(&rhs);
					value_destroy(&lhs);
					double result=evaluate_bin_op_numeric(lhs_n,rhs_n,node->operator);
					value_init_number(dst,result);
				}
				break;
				case AstOperatorCall:
					evaluate_call(dst,lhs,rhs,call_depth+1);
					break;
			}
		}
			break;
		case AstTagDefinition:
			value_init_function(dst,node->definition.body,up);
			break;
		case AstTagUpvalueRef:
			upvalue_get(up,dst,node->upvalue_ref_index);
			break;
		case AstTagNativeFunction:
			value_init_native_function(dst,node->native_function);
			break;
		case AstTagParentFrameArgRef:	
		case AstTagIdentifier:
			panic_here();
	}
}

static void evaluate_call(Value* dst,Value lhs,Value rhs,int call_depth){
	assert(call_depth<MAX_CALL_DEPTH,"call depth limit exceeded\n");
	static int function_call_id=0;
	int this_call;
	switch (value_type(&lhs)){
		case ValueTagFunction:{
			AstNode* body;
			UpValueChain* up;
			value_read_function(&lhs,&body,&up);
			#define DEBUG_PRINT_CONDITION 1 //body->tag != AstTagDefinition
			if(DEBUG_PRINT_CONDITION){
				this_call= ++function_call_id;
				printf("\n###################################\n### evaluate function call %4d ###\n###################################\n",this_call);
				printf("upvalues : ");
				upvalue_print_short(up);
				printf("\nargument : ");
				value_print_short(&rhs);
				printf("\nbody:\n");
				ast_node_print(stdout,body);
			}
			up=upvalue_push(up,&rhs);//up is now owned
			value_destroy(&rhs);
			value_destroy(&lhs);
			ast_evaluate_node(dst,body,up,call_depth+1);
			if(DEBUG_PRINT_CONDITION){
				printf("\n##########################\n### call %4d returned ###\n##########################\n",this_call);
				value_print_short(dst);
				printf("\n");
			}
			break;
		}
		case ValueTagNativeFunction:{
			NativeFunction* f;
			value_read_native_function(&lhs,&f);
			value_destroy(&lhs);
			f(dst,rhs);
			break;
		}
		case ValueTagNumber:
			panic("attempt to call number %f as functin\n",lhs.number);
	}
}


Value ast_evaluate(AstNode* ast){
	Value v;
	ast_evaluate_node(&v,ast,upvalue_new(),0);
	return v;
}

AstNode* ast_push_native_function(AstNode* ast,char* name,NativeFunction f){
	char* name_alloc=ualloc(strlen(name));
	strcpy(name_alloc,name);
	AstNode* nativeFn=ualloc(sizeof(AstNode));
	nativeFn->tag=AstTagNativeFunction;
	nativeFn->native_function=f;
	AstNode* argument=ast_node_alloc_ident(name_alloc);
	AstNode* definition=ast_node_alloc_definition(argument,ast);
	return ast_node_alloc_bin_op(definition,AstOperatorCall,nativeFn);
}
